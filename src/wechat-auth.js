/**
 * ------------------------------------------------------------------
 * 微信授权
 * @author SongJinDa <310676645@qq.com>
 * @date 17/3/26
 * ------------------------------------------------------------------
 */

class WeChatAuth {
  constructor (config) {
    let defaultConfig = {
              appid: '',
              component_appid: '',
              state: '',
              responseType: 'code',
              scope: 'snsapi_base ',
              getCodeCallback: () => {},
        onTokenGet: () => {},
  }
    this.config = Object.assign(defaultConfig, config)
  }

  openAuthPage (redirectUri = window.location.href) {
    let redirectUriArr = redirectUri.split('#')
    window.sessionStorage.setItem(`redirect_path_${this.config.appid}`, redirectUriArr[1])
    redirectUri = encodeURIComponent(redirectUriArr[0])
    this.removeAccessToken()
    this.removeRefreshToken()
    this.removeExpiredIn()
    this.removeAuthCode()
    let authPageBaseUri = 'https://open.weixin.qq.com/connect/oauth2/authorize'
    let authParams = `?appid=${this.config.appid}&component_appid=${this.config.component_appid}&redirect_uri=${redirectUri}&response_type=${this.config.responseType}&scope=${this.config.scope}#wechat_redirect`
    window.location.href = authPageBaseUri + authParams
  }

  setAuthCode (code) {
    if (!code) return false
    window.sessionStorage.setItem(`auth_code_${this.config.appid}` , code)
    return true
  }

  getAuthCode () {
    let codeValue = window.sessionStorage.getItem(`auth_code_${this.config.appid}`)
    if (!codeValue) return ''
    return codeValue
  }

  removeAuthCode () {
    window.sessionStorage.removeItem(`auth_code_${this.config.appid}`)
  }

  removeUrlCodeQuery () {
    let location = window.location
    let search = location.search
    if (search) {
      search = search.substr(1)
    }
    let href = location.origin
    let pathName = location.pathname
    if (pathName) {
      href += pathName
    }
    let searchArr = search.split('&').filter(item => {
          if (item.indexOf('code=') !== -1) {
      return false
    }
    if (item.indexOf('state=') !== -1) {
      return false
    }
    return true
  })
    if (searchArr.length > 0) {
      href += '?' + searchArr.join('&')
    }
    let hash = location.hash
    if (hash) {
      href += hash
    }
    window.location.href = href
  }

  validTokenFound(){
    let validToken = this.getAccessToken();
    if (!validToken)
    {
      return false;
    }

    let currentTime = (new Date().getTime() / 1000);
    let expiredIn = this.getExpiredIn();
    if (expiredIn === undefined)
    {
      return false;
    }

    if (currentTime >= expiredIn)
    {
      return false;
    }

    return true;

  }

  setAccessToken (accessToken) {
    if (!accessToken) return false
    window.localStorage.setItem(`access_token_${this.config.appid}`, accessToken)
    return true
  }

  getAccessToken () {
    return window.localStorage.getItem(`access_token_${this.config.appid}`)
  }

  removeAccessToken () {
    window.localStorage.removeItem(`access_token_${this.config.appid}`)
  }


  setOpenId (openId) {
    if (!openId) return false
    window.localStorage.setItem(`open_id_${this.config.appid}`, openId)
    return true
  }

  getOpenId () {
    return window.localStorage.getItem(`open_id_${this.config.appid}`)
  }

  removeOpenId () {
    window.localStorage.removeItem(`open_id_${this.config.appid}`)
  }

  setRefreshToken (refreshToken) {
    if (!refreshToken) return false
    window.localStorage.setItem(`refresh_token_${this.config.appid}`, refreshToken)
    return true
  }

  getRefreshToken () {
    return window.localStorage.getItem(`refresh_token_${this.config.appid}`)
  }

  removeRefreshToken () {
    window.localStorage.removeItem(`refresh_token_${this.config.appid}`)
  }

  setExpiredIn (expiredIn) {
    if (!expiredIn) return 0;
    window.localStorage.setItem(`expired_in_${this.config.appid}`, expiredIn)
    return true
  }

  getExpiredIn () {
    return window.localStorage.getItem(`expired_in_${this.config.appid}`)
  }

  removeExpiredIn () {
    window.localStorage.removeItem(`expired_in_${this.config.appid}`)
  }

  next (next) {
    let self = this
    return (accessToken, refreshToken, expiredIn, openId, to) => {
      if (accessToken) {
        self.setAccessToken(accessToken)
        self.setRefreshToken(refreshToken)
        self.setExpiredIn(expiredIn)
        self.setOpenId(openId)
        to
            ? next(to)
            : next()
      } else {
        self.removeAccessToken()
        self.removeRefreshToken()
        self.removeExpiredIn()
        self.removeOpenId()
        to && next(to)
      }
      self.removeAuthCode()
    }
  }

  next1 (next) {
    let self = this
    return (userInfo, to) => {
      if (userInfo) {
        to
            ? next(to)
            : next()
      } else {
        to && next(to)
      }
    }
  }

  getCodeCallback (next) {
    return this.config.getCodeCallback(this.getAuthCode(), this.next(next), window.sessionStorage.getItem(`redirect_path_${this.config.appid}`))
  }

  onTokenGet (next,redirectUri = window.location.href) {
    console.log("next to !!!!!!"+ redirectUri)
    if (redirectUri.includes("http://") || redirectUri.includes("https://"))
    {
      let redirectUriArr = redirectUri.split('#')
      window.sessionStorage.setItem(`redirect_path_${this.config.appid}`, redirectUriArr[1])
    }
    else
    {
      window.sessionStorage.setItem(`redirect_path_${this.config.appid}`, redirectUri)
    }

    return this.config.onTokenGet(this.getAccessToken(), this.getOpenId(), this.next1(next), window.sessionStorage.getItem(`redirect_path_${this.config.appid}`))
  }
}

export default WeChatAuth
